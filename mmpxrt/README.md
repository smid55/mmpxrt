# mmpxrt
Raytracing code for x-ray spectrometers with (not only) mossaic crystals
Written 2017-2020, Michal Smid

This is a raytracing code for design and analysis of x-ray spectrometers. Useful tool for (rather) experienced physicist. For more info about distribution, installation, usage, etc. of this package, please see attached manual.

All files related to the code are available at:
https://gitlab.hzdr.de/smid55/mmpxrt

The packaging was done by using this manual: https://packaging.python.org/tutorials/packaging-projects/
