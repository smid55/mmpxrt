#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019

@author: michal
"""
from mmpxrt import mmpxrt
import numpy as np
import pickle
import matplotlib.pyplot as plt


#Phelix Ge 400
detRots=np.array([60,65,69,69.5,70,70.5,71,75,80])

for detRot in detRots:
    name='Phelix-Ge400-detRot'+str(detRot)
    name='Phelix-Ge400-detRot{:04.1f}'.format(detRot)
#if 1:
    p=mmpxrt.init()
    p['source']['EcentralRay']=8127
    p['source']['EmaxBandwitdth']=1000
    
    
    p['crystal']['d2']=2.82874
    p['crystal']['mosaicity']=0
    
    p['crystal']['rockingCurveFWHM']=31/60/60/180*np.pi
    
    p['crystal']['width']=15
    p['crystal']['length']=50
    p['crystal']['radius_l']=400
    p['crystal']['radius_w']=200
    p['crystal']['crystalPeakReflectivity']=0.80
    p['geometry']['detectorPxSize'] = 50e-6
    p['crystal']['thickness']=0
    
    p['geometry']['detRot']=70
    p['geometry']['evaluation_width']=2
    p['geometry']['detRot']=detRot
    p['geometry']['CrystalSource']=300
    braggTheta=np.arcsin(12398/p['source']['EcentralRay']/p['crystal']['d2'])
    fV=((0.5)*p['crystal']['radius_w'])/np.sin(braggTheta);
    p['geometry']['CrystalDetector']=np.abs(1/(1/fV-1/p['geometry']['CrystalSource']));
    p['geometry']['detectorLength']=120
    p['geometry']['detectorWidth']=0.5
          
    p['simulation']['numraysE']=3 #short run
    p['simulation']['numraysE']=5 #long run
    p['simulation']['name']=name
    p['simulation']['num_processes']=10
    p['simulation']['progressmod']=10
    
    rrrs = mmpxrt.spectrometer(p)
    rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
    p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )
    
    mmpxrt.spectrometer_evaluate(p,rrrs)

if False:
    fig = plt.figure()
    fig= plt.figure(figsize=(8,8))
    mmpxrt.showDispersion(p,rrrs['broad'],None,True)

