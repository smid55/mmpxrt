#!/usr/bin/env python3
from mmpxrt import mmpxrt

p=mmpxrt.init()
p['simulation']['numraysE']=4
p['simulation']['name']="HOPG-flat-3"

p['source']['EcentralRay']=8500
p['source']['EmaxBandwidth']=4000

p['crystal']['d2']=6.708
p['crystal']['mosaicity']=0.8
p['crystal']['width']=20
p['crystal']['length']=50
p['crystal']['maxThickness']=2
p['crystal']['thickness']=-1

p['geometry']['detRot']=-1
p['geometry']['CrystalSource']=250
p['geometry']['CrystalDetector']=250
p['geometry']['evaluation_width']=20
p['geometry']['detectorLength']=200
p['geometry']['detectorWidth']=30

rrrs = mmpxrt.spectrometer(p)
mmpxrt.spectrometer_evaluate(p,rrrs)
