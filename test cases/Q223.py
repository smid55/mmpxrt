#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019

@author: michal
"""
from mmpxrt import mmpxrt
import numpy as np
import pickle
import matplotlib.pyplot as plt

name='Q223'
if True:
    p=mmpxrt.init()
    p['source']['EcentralRay']=8047
    p['source']['EmaxBandwidth']=1400
    
    
    p['crystal']['d2']=2.029
    p['crystal']['mosaicity']=0
    
    p['crystal']['rockingCurveFWHM']=23.1/60/60/180*np.pi
    
    p['crystal']['width']=18
    p['crystal']['length']=34
    p['crystal']['radius_l']=150
    p['crystal']['radius_w']=150
# int.r.=13.96μrad, fwhm=15.5", peak refl.=0.11    
    p['crystal']['crystalIntegratedReflectivity']=13.96e-6
    p['geometry']['detectorPxSize'] = 3e-6
    p['crystal']['thickness']=0
    
    p['geometry']['detRot']=0
    p['geometry']['evaluation_width']=2
    
    p['geometry']['CrystalSource']=550
    braggTheta=np.arcsin(12398/p['source']['EcentralRay']/p['crystal']['d2'])
    fV=((0.5)*p['crystal']['radius_w'])/np.sin(braggTheta);
    p['geometry']['CrystalDetector']=np.abs(1/(1/fV-1/p['geometry']['CrystalSource']));
    p['geometry']['detectorLength']=28
    p['geometry']['detectorWidth']=5
          
    p['simulation']['numraysE']=5
    
    p['simulation']['name']=name
    p['simulation']['num_processes']=3
    p['simulation']['progressmod']=50
    
    rrrs = mmpxrt.spectrometer(p)
    #rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
    #p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )
    
    mmpxrt.spectrometer_evaluate(p,rrrs)

if False:
    fig = plt.figure()
    fig= plt.figure(figsize=(8,8))
    mmpxrt.showDispersion(p,rrrs['broad'],None,True)

