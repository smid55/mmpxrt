#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019

@author: michal
"""
from mmpxrt import mmpxrt
import numpy as np
import pickle
import matplotlib.pyplot as plt

#Ge 220
detRots=np.array([-40,-30,-25,-20, -15, -10, -5])
detRots=np.array([-40,-25,-20, -15, -5])
rerun=1
if 1 :
    for detRot in detRots:
        name='Ge220-detRot{:03d}'.format(detRot+100)
        print(name)
        p=mmpxrt.init()
        p['source']['EcentralRay']=8247
        p['source']['EmaxBandwitdth']=1000
        
        p['source']['size']=0
        p['crystal']['d2']=4.00
        p['crystal']['mosaicity']=0
        
        p['crystal']['rockingCurveFWHM']=31/60/60/180*np.pi
        
        p['crystal']['width']=25
        p['crystal']['length']=50
        p['crystal']['radius_l']=1500
        p['crystal']['radius_w']=150
        p['crystal']['crystalPeakReflectivity']=0.80
        p['geometry']['detectorPxSize'] = 25e-3
        p['crystal']['thickness']=0
        p['crystal']['integrated_reflectivity']=1
        
        p['geometry']['evaluation_width']=2
        p['geometry']['detRot']=detRot
        p['geometry']['CrystalSource']=300
        braggTheta=np.arcsin(12398/p['source']['EcentralRay']/p['crystal']['d2'])
        fV=((0.5)*p['crystal']['radius_w'])/np.sin(braggTheta);
        p['geometry']['CrystalDetector']=np.abs(1/(1/fV-1/p['geometry']['CrystalSource']));
        p['geometry']['detectorLength']=120
        p['geometry']['detectorWidth']=1
              
        p['simulation']['numraysE']=3 #short run
        p['simulation']['numraysE']=6 #long run
        p['simulation']['name']=name
        p['simulation']['num_processes']=12
        p['simulation']['progressmod']=10
    
        p['geometry']['detectorPxSize'] = 10e-3
        p['geometry']['detectorLength']=40
        p['geometry']['detectorWidth']=0.2
        p['simulation']['name']=name
        p['simulation']['comment']="detector rotation = {:d}°".format(detRot)
    
    
        if rerun:
            rrrs = mmpxrt.spectrometer(p)
        else:
            rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
            p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )
        mmpxrt.spectrometer_evaluate(p,rrrs)

     
# %%  draw nice geometry
broad_rrr=rrrs['broad']
fig= plt.figure(figsize=(8,4))
ax = fig.add_subplot(111)
mmpxrt.drawSideView(p,broad_rrr,ax)
plt.axis('equal')
plt.ylim()        
plt.savefig('Ge220_geometry.png',dpi=300)
        
        # %%
if 0:
    fig = plt.figure()
    fig= plt.figure(figsize=(8,18))
    ax = fig.add_subplot(111)
    p['geometry']['detectorPxSize'] = 10e-3
    p['geometry']['detectorLength']=15
    p['geometry']['detectorWidth']=7
    #mmpxrt.showDispersion(p,rrrs['broad'],None,True)
    mmpxrt.drawDetectorStripes(p,rrrs['broad'],ax,1)
