#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019
@author: michal, xiayun
"""
import sys
from mmpxrt import mmpxrt
import numpy as np
import pickle
import matplotlib.pyplot as plt

p=mmpxrt.init()
rerun=0
name="HOPG-flat-3"
p['source']['EcentralRay']=8500
p['source']['EmaxBandwidth']=4000

p['crystal']['d2']=6.708
p['crystal']['mosaicity']=0.8

p['crystal']['width']=20
p['crystal']['length']=50
p['crystal']['radius_l']=1e9
p['crystal']['radius_w']=1e9
p['crystal']['crystalPeakReflectivity']=0.45
p['crystal']['maxThickness']=2
p['crystal']['thickness']=-1

p['geometry']['detRot']=-1
p['geometry']['CrystalSource']=250
p['geometry']['CrystalDetector']=250
p['geometry']['evaluation_width']=20
p['geometry']['detectorLength']=200
p['geometry']['detectorWidth']=30
p['geometry']['detectorPxSize'] = 13.5e-3
      
p['simulation']['numraysE']=6
p['simulation']['name']=name
p['simulation']['comment']="simulation of simple spectrometer"
p['simulation']['num_processes']=12

if rerun:
    rrrs = mmpxrt.spectrometer(p)
else:    
    rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name, "rb" ) )
    p=pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )

mmpxrt.spectrometer_evaluate(p,rrrs)  


# %%
rrr=rrrs['mono']
mmpxrt.evaluateMono(p,rrr)