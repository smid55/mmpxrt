#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 10:02:19 2019

@author: michal
"""
import sys
from mmpxrt import mmpxrt
import mmpxrtSAXSRC
import pickle
import numpy as np
import matplotlib.pyplot as plt

rerun=1
L=0;
E=8150;
ThBr=-1;

#all for line
ThBrs=np.array([ 12.7  , 12.8  , 12.9  , 12.95 , 13.   , 13.02 ,       13.025, 13.05 , 13.075, 13.1  , 13.11 , 13.125, 13.15 , 13.175,       13.19 , 13.2  , 13.25 , 13.3  , 13.35 , 13.4  , 13.5])

ThBrs=np.array([13.1 , 12.9  , 13.,13.2  ,13.3 ,12.5, 13.7 ])
# only for line, while show is already calculated.
ThBrs=np.array([12.95 , 13.02 ,       13.025, 13.05 , 13.075, 13.1  , 13.11 , 13.125, 13.15 , 13.175,       13.19 , 13.2  , 13.25 , 13.35 , 13.4  , 13.5])
#to show by SAXSmirror-rc-overview.py
ThBrs=np.array([13.1 , 12.9  , 13.,13.2  ,13.3 ,12.5, 13.7 ])

ThBrs=ThBrs/180*np.pi
#ThBrs=np.array([12.7,13.6,13.7,12.6])/180*np.pi

ThEff=np.zeros(ThBrs.size)
i=0;
for ThBr in ThBrs:
    name="SAXS-L{:03.1F}m-E{:d}eV-Th{:03.2F}".format(L,E,ThBr/np.pi*180)
    print (name)
    ##doing Lund Xanes:
    parameters=mmpxrt.init()
    parameters['source']['EcentralRay']=E
    parameters['source']['EmaxBandwidth']=300
#    parameters['source']['divergenceRing']=0.4358/180*np.pi #AgBe
    parameters['simulation']['comment']='rc'
    
    #numerical parameters, default values should be ok for most cases. 
    #When details of mossaic stuff at non-optimal angles are important, having higher values makes more precision, like e.g. width=30, numpoints=50 was used for mossaicity section of the CPC paper
    parameters['simulation']['numerical_mossaic_phimap_width']=30
    parameters['simulation']['numerical_mossaic_phimap_numpoints']=50
    
    parameters['crystal']['d2']=6.708
#"We try our best with mosaicity but we would rather guaranty 0,08+/-0,02 degree , than promise you exact 0,05."
    parameters['crystal']['mosaicity']=0.08
    
    parameters['crystal']['width']=3
    parameters['crystal']['length']=3
    parameters['crystal']['radius_width']=5500
    parameters['crystal']['radius_length']=1e9
    parameters['crystal']['crystalPeakReflectivity']=0.45
    parameters['crystal']['maxThickness']=30e-3
    parameters['crystal']['thickness']=-1
#    parameters['crystal']['gap']=3 #mm
    
    parameters['geometry']['detRot']=0
    parameters['geometry']['evaluation_width']=45
    
    parameters['geometry']['CrystalSource']=1230
    parameters['geometry']['CrystalDetector']=250
    parameters['geometry']['detectorLength']=10
    parameters['geometry']['detectorWidth']=10
    parameters['geometry']['ThBragg0']=ThBr
    parameters['simulation']['numraysE']=-1
    parameters['simulation']['numraysM']=1e2 #used
    parameters['simulation']['numraysR']=1e1 #not used
    parameters['simulation']['name']=name 
    parameters['simulation']['num_processes']=5
    p=parameters   
    #### simulation
    
    if rerun:
        rrrs = mmpxrtSAXSRC.SAXSmirror(p)
    else:
        rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
        p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )    
        p['geometry']['detectorLength']=10
        p['geometry']['detectorWidth']=5
    eff=mmpxrtSAXSRC.SAXSmirror_evaluate(p,rrrs,0,-1)[0]
        
    ThEff[i]=eff
    i=i+1;
