"""
Created on Tue Aug 20 15:35:05 2019

@author: Michal Smid, IoP ASCR 2015-2017, HZDR 2018-2019
                ||/
 M~M~P~X~R~T   |/|
 Michal       ///-----------<
   Mosaic    |/| \
     Python |//   \ 
       X-          \
         Ray        \ 
           Tracing  ==


           S A X S    Rocking Curve ideas
           mirror
           

Additional Library to simulate a HOPG SAXS mirror. 
Not to be run separately, to be called by e.g. SAXSmirror-rc-calc.py

"""

import pickle
from mpl_toolkits.mplot3d import Axes3D
import random
import matplotlib.pyplot as plt
from astropy.io import ascii
import numpy as np
import time
import sys
import multiprocessing
import pdb
import math
from mmpxrt import mmpxrt

#import numba as nb
    
def SAXSmirror(p):
    p['source']['continuumAdd']=True
    p['source']['continuumMarks']=False
    p['source']['continuumAddedRatio']=0.
    
    p['source']['divergenceAutomatic']=False
    p['source']['divergenceFWHM']=0
    
    p['simulation']['collectLostBeams']=False
    
    if p['simulation']['numraysE']>0:
        p['simulation']['numraysB']=round(10**(p['simulation']['numraysE']))
        p['simulation']['numraysM']=round(10**(p['simulation']['numraysE']-1))

    windowY=30
#see if I need this    effectiveCalc=True#may make efficiency calculation wrong for thin crystals
    
    p=mmpxrt.geometry(p)
    sim=p['simulation']
    s=p['source']
    sg=p['sg']
    nump=sim['num_processes'];
    s['rOffset']=[0,0,0]
    sg['windowX']=10
    sg['windowY']=10
    start = time.time()   

    
    
    s['secondBeamRatio']=0
    s['showrealspatial']=False
    s['continuum']=False
    s['rOffsetRatio']=0.
    s['rOffset']=[0,0,0]
    
    ## Flat run
    print( '\nGoing to raytrace (monochromatic, flat)')
    sg['numrays']=p['simulation']['numraysM']
    if nump>1:
        sg['numrays']=int(np.round(p['simulation']['numraysM']/nump))
        monorrr = mmpxrt.raytrace_multiprocess(p,nump)
    else:
        monorrr = mmpxrt.raytrace(p,None,None)



    ringrrr = monorrr


    end = time.time()   




    spectrorrr={}
    spectrorrr['elapsedTime']=end-start
    spectrorrr['mono']=monorrr
    spectrorrr['ring']=ringrrr
    
    fn=p['simulation']['out_data_directory']  +'mmpxrt_results_' +p['simulation']['name']
    pickle.dump( spectrorrr, open( fn, "wb" ) )
    fn=p['simulation']['out_data_directory']  +'mmpxrt_parameters_' +p['simulation']['name']
    pickle.dump( p, open( fn, "wb" ) )
 
    return spectrorrr
    
    
    
def SAXSmirror_evaluate(p,spectrorrr,quarterring,climmax):
# broadband
#    rayres=broadrrr.rayres
 #   E0s=broadrrr.E0s
  #  numrays=broadrrr.numrays
   # colores=broadrrr.colores
    so=p['source']
    c=p['crystal']
    sg=p['sg']
    mono_rrr=spectrorrr['mono']
    ring_rrr=spectrorrr['ring']

    elapsedTime=spectrorrr['elapsedTime']
    evalu={}
    p['evalu']=evalu    
    ev=p['evalu']
   
    fig= plt.figure(figsize=(16,7))
            
    # verified numbers:
    nl='\n'
    nl='\\\\'
    s=' \t'
    
    ii=""
    ii=ii+ '{{\\huge '+ p['simulation']['name'] +' }}'+ nl
    ii=ii +p['simulation']['comment']+ nl+nl
    ii=ii +'central E: \t {:2.0f}'.format(so['EcentralRay'])+  ' eV '+ nl
    ii=ii +'number of rays: \t  {:2.0e} + {:2.0e}'.format(ring_rrr['numrays'],mono_rrr['numrays']) + nl
    if (elapsedTime>300):
        ii=ii +'time: \t {:2.0f} min., {:2.0f} r/s'.format(elapsedTime/60,(mono_rrr['numrays'])/elapsedTime) + nl
    else:
        ii=ii +'time: \t {:2.0f} s,  {:2.0f} r/s'.format(elapsedTime,(mono_rrr['numrays'])/elapsedTime) + nl
    ii=ii +nl+'{{\\bf Geometry }}' +nl
    ii=ii+ '$d_{{\\textrm{{SC}}}}$: \t {:2.2f}'.format(sg['Edist'])+ ' mm'+ nl
    ii=ii+ '$d_{{\\textrm{{CD}}}}$: \t {:2.2f}'.format(sg['Edist_dect'])+ ' mm' +nl
    ii=ii +'$\\theta_{{\\textrm{{Bragg}}}}$ : \t {:2.2f}'.format(sg['ThBragg']/np.pi*180) +'$^\\circ$' +nl
    ii=ii +'gap between crystals: \t {:2.0f} mm'.format(p['crystal']['gap']) +nl
#    ii=ii +'ring $2\\theta$: \t {:2.4f} °'.format(p['source']['divergenceRing']/np.pi*180) +nl
    ii=ii+ nl 
    
    mmpxrt.evaluateMono(p,mono_rrr)
    
    
    ax = fig.add_subplot(121)
    plt.title('Detected ring (on 100um pixels)')
        
    l=p['geometry']['CrystalSource']+p['geometry']['CrystalDetector']    
    if np.size(so['divergenceRing'])==1:
        r=np.sin(so['divergenceRing'])*l
        circle1 = plt.Circle((0, 0), r*1.05, linewidth=0.5,color=[0.7,0,0],fill=False)
        ax.add_artist(circle1)
    maximum = drawDet(p,ring_rrr,ax,55e-3,-1)    
    if quarterring:
        plt.title('Detected 1/4 ring (on 100um pixels)')
        plt.xlim(0,9)
        plt.ylim(0,12)
    
#getting efficiency from ring simulation    
    ## efficiency
    rrr=ring_rrr
    reflectedratio=rrr['effdet']/rrr['effcnt']
    efficiency=reflectedratio
    p['evalu']['efficiency']=efficiency
    
    ii=ii+ nl 
    rOff=np.linalg.norm(so['rOffset'])
    
    if c['mosaicity']==0:
        c['crystalPeakReflectivity']=c['crystalIntegratedReflectivity']/c['rockingCurveFWHM']
    
    totalEfficiency=p['evalu']['efficiency']*c['crystalPeakReflectivity']
    
    ii=ii +nl+ 'efficiency of given ring: \t {:2.4f} \%'.format(totalEfficiency*100)
    
    #ax = fig.add_subplot(122)
  #  if climmax==-2:
 #       climmax=1800*0.38/totalEfficiency
#    drawDet(p,mono_rrr,ax,55e-3,climmax)
    
    
    if np.size(so['divergenceRing'])==1:
        circle2 = plt.Circle((0, 0), r*1.05, linewidth=0.5,color=[0.7,0,0],fill=False)
        ax.add_artist(circle2)
    plt.plot()
    plt.title('Detected flat-field')
    
    
    # print info
    from matplotlib import rc
    #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    #rc('font',**{'family':'serif','serif':['Palatino']})
    rc('text', usetex=True)
    
    ax = fig.add_subplot(133)
    t=plt.text(-0.1,1,ii,transform=ax.transAxes,FontSize=12,VerticalAlignment='top')
    ax.axis('off')
    ax.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

  #  ax = fig.add_subplot(247, projection='3d')
 #   mmpxrt.plotoneview(p,mono_rrr,100,ax)
#    plt.title('3D view')

    plt.savefig('mmpxrt_' +p['simulation']['name']+'.png' , bbox_inches ='tight',dpi=140)
    fn=p['simulation']['out_data_directory']  +'mmpxrt_parameters_' +p['simulation']['name']
    pickle.dump( p, open( fn, "wb" ) )
    return np.array([totalEfficiency,maximum*totalEfficiency])
    


def drawDet(p,rrr,ax,pxsize,climmax):
    colores=rrr['colores']
    rayres=rrr['rayres']
    E0s=rrr['E0s']
    ev=p['evalu']
    Sdetector=p['sg']['Sdetector']

    numrays=np.size(colores)
    posCont=np.full((numrays,2),np.nan)    
    posR=np.full((numrays,2),np.nan)    
    po=np.array([0.,0.],float)
    for i in np.arange(np.shape(rayres)[0]):
          absPos=rayres[i,2,:]
       #projection
          po[0]=absPos[1]#y in 3D becomes x in 2D
          po[1]=((absPos[0]-Sdetector[0])**2 + (absPos[2]-Sdetector[2])**2) ** (1/2)#distance from detector center
          if (absPos[0]-Sdetector[0])<0:#see if it was 'front' or 'behind' the detector          
             po[1]=po[1]*-1
          
          if colores[i]==1:
              posR[i,:]=po     
          
    if (np.nansum(np.nansum(posR))==0):
        return
    
    ## get the optimal ranges
    pC=posR
    xs=(pC[:,0])
    ys=(pC[:,1])
    xs2=xs[np.logical_not(np.isnan(xs))]
    ys2=ys[np.logical_not(np.isnan(ys))]    
    xmax=np.max(xs2)
    ymax=np.max(ys2)

    
    if p['geometry']['detectorWidth']>0:
        windowX=p['geometry']['detectorWidth']/2
    windowY=p['geometry']['detectorLength']/2

    
    cnt=np.shape(posCont)[0]
    windowY=0.1
    windowX=2
    stepX=0.1
    stepY=0.002 # to see nice picture
    stepY=0.01  # to integrate
    
#detail
    windowY=0.02
    windowX=0.5
    stepX=0.1
    stepY=0.001  
    
    aspe='auto'
    ## plt.plotting 
    xmax=xmax*-1
    #ymax=ymax*-1
    #ymax=0
    cntrsX=np.arange(-1*windowX,windowX,stepX)
    cntrsY=np.arange(ymax-1*windowY,ymax+windowY,stepY)
    
    edX=cntrsX+stepX/2
    edY=cntrsY+stepY/2
    #cntrs=[cntrsX,cntrsY]
    eds=[edX,edY]
    nR = np.histogram2d(posR[:,0],posR[:,1],eds)[0]
    nR[0,0]=0
    nR[np.shape(nR)[0]-1,np.shape(nR)[1]-1]=0
    nR[0,np.shape(nR)[1]-1]=0
    nR[np.shape(nR)[0]-1,0]=0
    nR2=np.flip(nR,1)
    maximum=np.max(nR2)
    #nR2=np.transpose(nR)
    plt.imshow(nR2,extent=(windowY,-1*windowY,windowX,-1*windowX),aspect=aspe,origin='lower')
    #plt.imshow(nR)
    
    
    ## profiles
    
    su2=np.sum(nR,0)
    su2y=np.sum(nR,1)
    su2[0]=0
    su2[np.size(su2)-1]=0
    su2y[0]=0
    su2y[np.size(su2y)-1]=0
    
    su2=su2/np.max(su2)*0.5*windowX-windowX
    su2y=su2y/np.max(su2y)*0.5*windowY-windowY
    
    plt.plot(cntrsY-ymax,np.append(np.nan,su2),'w',linewidth=1)
    plt.plot(np.append(np.nan,su2y),cntrsX,'w',linewidth=1)
    
    #selected profile
    evalSelectY=p['geometry']['evaluation_width']    
    es2=evalSelectY/2*1e3
    wy=windowY*1e3*1.1
    
    vyy=np.squeeze(np.asarray(p['evalu']['verticalSelectPSF'][1,:]))
    vyy=(vyy/np.nanmax(vyy)*0.5*windowX-windowX)*1e3
    
    plt.xlabel('d [mm]')
    plt.ylabel('y [mm]')
    #plt.colorbar()
    if climmax>0:
        plt.clim(0,climmax)
    #plt.xlim(-1*windowY,windowY)
    #plt.ylim(-1*windowX,windowX)
    return maximum/numrays
