#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019

@author: Michal Smid
"""
#import mmpxrtOld as mmpxrt

from mmpxrt import mmpxrt
import numpy as np
import matplotlib.pyplot as plt
import pickle

rerun=1 #run all the simulations, otherwise would try to load previous results
dscs=np.arange(580,770,10) #Dsc to be simulated
#dscs=np.array([740]) #... to try just one distnace now
res1=np.zeros(dscs.size) #arrays for simulated resolutions
res2=np.zeros(dscs.size)
res3=np.zeros(dscs.size)
for di,dsc in enumerate(dscs): 
    p=mmpxrt.init()
    name='mossDefoc-dsc-{:2d}'.format(dsc);
    p['source']['EcentralRay']=9040
    p['source']['EmaxBandwidth']=1800
    p['source']['divergenceFWHM']=-1 #automatic
    
    p['crystal']['d2']=6.708
    p['crystal']['mosaicity']=0.8
    
    p['crystal']['width']=20
    p['crystal']['length']=40
    p['crystal']['radius_l']=1e9
    p['crystal']['radius_w']=115
    p['crystal']['crystalPeakReflectivity']=0.45
    p['crystal']['maxThickness']=30e-3
    p['crystal']['thickness']=-1 #use exponential dist.
    
    p['geometry']['detRot']=0
    p['geometry']['evaluation_width']=3
    p['geometry']['CrystalSource']=dsc
    p['geometry']['CrystalDetector']=680
    p['geometry']['evaluation_width']=3
    p['geometry']['detectorLength']=40
    p['geometry']['detectorPxSize'] = 5e-3
          
    p['simulation']['numraysE']=-1
    p['simulation']['numraysB']=1e3 #rays for broadband sim.
    p['simulation']['numraysM']=1e1 #rays for monochrom.sim.
    p['simulation']['name']=name
    p['simulation']['comment']='study of mosaic defocusing'    
    p['simulation']['num_processes']=1
    
    print(name)
    if rerun:
        rrrs = mmpxrt.spectrometer(p) #the simualtion itself
    else:
        rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
        p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )
    
    mmpxrt.spectrometer_evaluate(p,rrrs)
    dispersionLinearCentral=p['evalu']['dispersionLinearCentral']
    res1[di]=p['evalu']['verticalSpreadRMS']*dispersionLinearCentral
    res2[di]=p['evalu']['verticalSpreadFWHM']*dispersionLinearCentral
    res3[di]=p['evalu']['verticalSpreadFWHMNarrow']*dispersionLinearCentral
    
# %% Drawing the output figure
plt.plot([680,680],[0,10],linewidth=3,color=[0.5,0.5,0.5])
plt.plot(dscs,res1,label='RMS')
plt.plot(dscs,res2,label='FWHM, wide')
plt.plot(dscs,res3,'-',label='FWHM, narrow')
plt.grid()
plt.legend()
plt.xlabel("source-crystal distance $d_\mathtt{{SC}}$ [mm]")
plt.ylabel("spectral resolution [eV]")
plt.ylim(0,25)
plt.xlim(580,760)
plt.text(680,10.5,'$d_\mathtt{{CD}}$')
plt.xticks(np.arange(580,780,20))
plt.savefig('Fig7.png' , bbox_inches ='tight',dpi=200)
