#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 13:32:17 2019

@author: michal, xiayun
"""
import sys
from mmpxrt import mmpxrt
import numpy as np
import pickle
import matplotlib.pyplot as plt


#simulation of HOPG spectrometer at Draco facility
if 1:
    p=mmpxrt.init()
    p['source']['EcentralRay']=8988
    p['source']['EmaxBandwidth']=1600
    
    p['crystal']['d2']=6.708
    p['crystal']['mosaicity']=0.8
    
    p['crystal']['width']=20
    p['crystal']['length']=40
    p['crystal']['radius_l']=1e9
    p['crystal']['radius_w']=115
    p['crystal']['crystalPeakReflectivity']=0.45
    p['crystal']['maxThickness']=30e-3
    p['crystal']['thickness']=-1
    
    p['geometry']['detRot']=0
    p['geometry']['CrystalSource']=560;
    p['geometry']['CrystalDetector']=590;
    p['geometry']['evaluation_width']=3

    name='Microfocus-HOPG-m{:2.1f}'.format(p['crystal']['mosaicity']);

    p['geometry']['detectorLength']=40
          
    p['simulation']['numraysE']=3 #short
    p['simulation']['numraysE']=5 #long
    p['simulation']['name']=name
    p['simulation']['num_processes']=4
    p['geometry']['detectorPxSize'] = 13.5e-3
    
    print(name)
    rrrs = mmpxrt.spectrometer(p)
 #   rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name, "rb" ) )
  #  p=pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )
    
    mmpxrt.spectrometer_evaluate(p,rrrs)  