#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 10:02:19 2019

@author: michal


Script to generate overview figure of rocking HOPG mirror.
It loads simulation results calculted by SAXSmirror-rc-calc.py, i.e. that
script has to be run first!
"""
#import mmpxrt
import sys
from mmpxrt import mmpxrt
import mmpxrtSAXSRC
import pickle
import numpy as np
import matplotlib.pyplot as plt

def drawDet(p,rrr):
    colores=rrr['colores']
    rayres=rrr['rayres']
    E0s=rrr['E0s']
    ev=p['evalu']
    Sdetector=p['sg']['Sdetector']

    numrays=np.size(colores)
    posCont=np.full((numrays,2),np.nan)    
    posR=np.full((numrays,2),np.nan)    
    po=np.array([0.,0.],float)
    for i in np.arange(np.shape(rayres)[0]):
          absPos=rayres[i,2,:]
       #projection
          po[0]=absPos[1]#y in 3D becomes x in 2D
          po[1]=((absPos[0]-Sdetector[0])**2 + (absPos[2]-Sdetector[2])**2) ** (1/2)#distance from detector center
          if (absPos[0]-Sdetector[0])<0:#see if it was 'front' or 'behind' the detector          
             po[1]=po[1]*-1
          
          if colores[i]==1:
              posR[i,:]=po     
          
    if (np.nansum(np.nansum(posR))==0):
        return
    ## get the optimal ranges
    pC=posR
    xs=(pC[:,0])
    ys=(pC[:,1])
    xs2=xs[np.logical_not(np.isnan(xs))]
    ys2=ys[np.logical_not(np.isnan(ys))]    
    xmax=np.max(xs2)
    ymax=np.max(ys2)

    
    if p['geometry']['detectorWidth']>0:
        windowX=p['geometry']['detectorWidth']/2
    windowY=p['geometry']['detectorLength']/2

    
    cnt=np.shape(posCont)[0]
    windowY=0.1
    windowX=2.2
    stepX=0.07
    stepY=0.007
    aspe='auto'
    ## plt.plotting 
    xmax=xmax*-1
    cntrsX=np.arange(-1*windowX,windowX,stepX)
    cntrsY=np.arange(ymax-1*windowY,ymax+windowY,stepY)
    
    edX=cntrsX+stepX/2
    edY=cntrsY+stepY/2
    eds=[edX,edY]
    nR = np.histogram2d(posR[:,0],posR[:,1],eds)[0]
    nR=nR/numrays
    nR[0,0]=0
    nR[np.shape(nR)[0]-1,np.shape(nR)[1]-1]=0
    nR[0,np.shape(nR)[1]-1]=0
    nR[np.shape(nR)[0]-1,0]=0
    nR2=np.flip(nR,1)
    maximum=np.max(nR2)
    plt.imshow(nR2,extent=(windowY,-1*windowY,windowX,-1*windowX),aspect=aspe,origin='lower')
    ## profiles
    su2=np.sum(nR,0)
    su2y=np.sum(nR,1)
    su2[0]=0
    su2[np.size(su2)-1]=0
    su2y[0]=0
    su2y[np.size(su2y)-1]=0
#    su3y=np.log10(su2y*1e4)
    
    su2=windowX - su2/np.max(su2)*0.4*windowX
    su2y=0.03 - su2y/np.max(su2y)*0.5*windowY
 #   su3y=su3y/np.max(su3y)*0.5*windowY-windowY
    
    plt.plot(cntrsY-ymax,np.append(np.nan,su2),'w',linewidth=1)
    plt.plot(np.append(np.nan,su2y),cntrsX,'w',linewidth=1)
    #plt.plot(np.append(np.nan,su3y),cntrsX,'r',linewidth=1)
    
    #selected profile
    evalSelectY=p['geometry']['evaluation_width']    
    es2=evalSelectY/2*1e3
    wy=windowY*1e3*1.1
    
    vyy=np.squeeze(np.asarray(p['evalu']['verticalSelectPSF'][1,:]))
    vyy=(vyy/np.nanmax(vyy)*0.5*windowX-windowX)*1e3
    
    return maximum/numrays


def getMax(p,rrr):
    colores=rrr['colores']
    rayres=rrr['rayres']
    E0s=rrr['E0s']
    ev=p['evalu']
    Sdetector=p['sg']['Sdetector']

    numrays=np.size(colores)
    posCont=np.full((numrays,2),np.nan)    
    posR=np.full((numrays,2),np.nan)    
    po=np.array([0.,0.],float)
    for i in np.arange(np.shape(rayres)[0]):
          absPos=rayres[i,2,:]
       #projection
          po[0]=absPos[1]#y in 3D becomes x in 2D
          po[1]=((absPos[0]-Sdetector[0])**2 + (absPos[2]-Sdetector[2])**2) ** (1/2)#distance from detector center
          if (absPos[0]-Sdetector[0])<0:#see if it was 'front' or 'behind' the detector          
             po[1]=po[1]*-1
          
          if colores[i]==1:
              posR[i,:]=po     
          
    if (np.nansum(np.nansum(posR))==0):
        return
    ## get the optimal ranges
    pC=posR
    xs=(pC[:,0])
    ys=(pC[:,1])
    xs2=xs[np.logical_not(np.isnan(xs))]
    ys2=ys[np.logical_not(np.isnan(ys))]    
    xmax=np.max(xs2)
    ymax=np.max(ys2)

    
    if p['geometry']['detectorWidth']>0:
        windowX=p['geometry']['detectorWidth']/2
    windowY=p['geometry']['detectorLength']/2

    
    cnt=np.shape(posCont)[0]
    windowY=1
    windowX=1.5
    stepX=0.05
    stepY=0.2
    aspe='auto'
    ## plt.plotting 
    xmax=xmax*-1
    cntrsX=np.arange(-1*windowX,windowX,stepX)
    cntrsY=np.arange(ymax-1*windowY,ymax+windowY,stepY)
    
    edX=cntrsX+stepX/2
    edY=cntrsY+stepY/2
    eds=[edX,edY]
    nR = np.histogram2d(posR[:,0],posR[:,1],eds)[0]
    nR=nR/numrays
    nR[0,0]=0
    nR[np.shape(nR)[0]-1,np.shape(nR)[1]-1]=0
    nR[0,np.shape(nR)[1]-1]=0
    nR[np.shape(nR)[0]-1,0]=0
    nR2=np.flip(nR,1)
    maximum=np.max(nR2)
    if 0:
        plt.imshow(nR2,extent=(windowY,-1*windowY,windowX,-1*windowX),aspect=aspe,origin='lower')
        #plt.xlim(0.01,-0.02)
        ## profiles
        su2=np.sum(nR,0)
        su2y=np.sum(nR,1)
        su2[0]=0
        su2[np.size(su2)-1]=0
        su2y[0]=0
        su2y[np.size(su2y)-1]=0
        
        su2=su2/np.max(su2)*0.5*windowX-windowX
        su2y=su2y/np.max(su2y)*0.5*windowY-windowY
        
    #    plt.plot(cntrsY-ymax,np.append(np.nan,su2),'w',linewidth=1)
        plt.plot(np.append(np.nan,su2y),cntrsX,'w',linewidth=1)
        
        #selected profile
        evalSelectY=p['geometry']['evaluation_width']    
        es2=evalSelectY/2*1e3
        wy=windowY*1e3*1.1
        
        vyy=np.squeeze(np.asarray(p['evalu']['verticalSelectPSF'][1,:]))
        vyy=(vyy/np.nanmax(vyy)*0.5*windowX-windowX)*1e3

    return maximum
    
# %% reading all to make the curve


ThBrs=np.array([13.1 , 12.9  , 13.,13.2  ,13.3 ,12.95 , 13.02 ,       13.025, 13.05 , 13.075, 13.1  , 13.11 , 13.125, 13.15 , 13.175,       13.19 , 13.2  , 13.25 , 13.35 , 13.4  , 13.5])

ThBrs=np.sort(ThBrs)
ThBrs=ThBrs/180*np.pi

ThEff=np.zeros(ThBrs.size)
ThMax=np.zeros(ThBrs.size)
i=0
L=0
E=8150
maximum=0
for ti,ThBr in enumerate(ThBrs):
    name="SAXS-L{:03.1F}m-E{:d}eV-Th{:03.2F}".format(L,E,ThBr/np.pi*180)
    print (name)
    
    rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
    p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )    
    #res=mmpxrtSAXSRC.SAXSmirror_evaluate(p,rrrs,0,-1)
    maximum=getMax(p,rrrs['ring'])
    totalEfficiency=p['evalu']['efficiency']*p['crystal']['crystalPeakReflectivity']
    ThEff[i]=totalEfficiency
    ThMax[i]=maximum*totalEfficiency
    i=i+1;
    
ThBrsWhole=ThBrs    
    
# %% drawing SELECTION OF PROFILES
from matplotlib import rc
rc('text', usetex=False)
fig= plt.figure(figsize=(10,8))

rerun=0
L=0;
E=8150;
ThBr=-1;
ThBrs=np.array([12.5,12.9,13.0,13.1,13.2,13.3,13.7])
ThBrs=np.array([12.5,12.9,13.0,13.1,13.2,13.3,13.7])
#ThBrs=np.array([13.1])

ThBrs=np.sort(ThBrs)
ThBrs=ThBrs/180*np.pi

import matplotlib.patches as mpatches

for ti,ThBr in enumerate(ThBrs):
    name="SAXS-L{:03.1F}m-E{:d}eV-Th{:03.2F}".format(L,E,ThBr/np.pi*180)
    print (name)
    
    rrrs=pickle.load(open( "datafiles/mmpxrt_results_"+name+"", "rb" ) )    
    p=  pickle.load(open( "datafiles/mmpxrt_parameters_"+name, "rb" ) )    
    #res=mmpxrtSAXSRC.SAXSmirror_evaluate(p,rrrs,0,-1)
    ax=plt.subplot(2,5,ti+1)
    maximum=drawDet(p,rrrs['ring'])
    
    plt.text(-0.013,-1.6,"{:0.1f}°".format(ThBr/np.pi*180),color='white',fontsize=15)
    plt.axis('off')
    plt.xlim(0.03,-0.09)
    plt.ylim(2,-2)
    #plt.colorbar()
    #plt.clim(0,0.02)
    res=[0,0]
    totalEfficiency=p['evalu']['efficiency']*p['crystal']['crystalPeakReflectivity']
    

    if ThBr==0.22863813201125716:
        scale=15
        h=0.015
        v=1.1
        arrow = mpatches.FancyArrowPatch((-0.01, v), (-0.09, v),mutation_scale=scale, color='white',lw=0)
        ax.add_patch(arrow)
        arrow = mpatches.FancyArrowPatch((-0.015, v),(0.03, v), mutation_scale=scale, color='white',lw=0)
        ax.add_patch(arrow)
        arrow = mpatches.FancyArrowPatch((h, 1), (h, -2),mutation_scale=scale, color='white',lw=0)
        ax.add_patch(arrow)
        arrow = mpatches.FancyArrowPatch((h, 0), (h, 2),mutation_scale=scale, color='white',lw=0)
        ax.add_patch(arrow)
        plt.text(-0.009,v-0.15,"120 μm",color='white',fontsize=15)
        plt.text(h-0.001,-1,"4 mm",color='white',fontsize=15,rotation=-90)

# % plotting the curve
    
#plt.subplot2grid((2,5),(1,2),colspan=3)
plt.subplot(position=[0.49,0.13,0.41,0.34])

ThBrs=ThBrsWhole
plt.plot(ThBrs/np.pi*180,ThEff/np.max(ThEff),'-',label='Efficiency')
plt.plot(ThBrs/np.pi*180,ThMax/np.max(ThMax),'-',label='Peak maximum')
#exp=pickle.load(open( 'rocking_curve.pickle', "rb" ))
#ex=exp[0]
#ex=ex+0.17
#plt.plot(ex,exp[4],'*-',label='exp. max.profs')
#plt.plot(ex,exp[2],'*-',label='exp. integrated')


x0=13.107
angles=np.arange(x0-0.8,x0+0.8,0.002)
Mozaicity=0.08
gamma=Mozaicity/2
pigamma=gamma*np.pi
lorentzs=1/(pigamma*(1+((angles-x0)/gamma)**2))
lorentzs=lorentzs/np.max(lorentzs)*1.
plt.plot(angles,lorentzs,'-',color=[0.5,0.5,0.5],linewidth=1,label='Lorentz, 0.08°')


plt.xlabel('crystal angle [°]')
plt.ylabel('efficiency [\\%]')
plt.xlim(12.9,13.5)

plt.legend()
plt.grid()
plt.savefig('SAXS_Theta-overviewRealThickness.png' , bbox_inches ='tight',dpi=200)
