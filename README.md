# mmpxrt
Raytracing code for x-ray spectrometers with (not only) mossaic crystals
Written 2017-2020, Michal Smid

This is a raytracing code for design and analysis of x-ray spectrometers. Useful tool for (rather) experienced physicist. For more info about distribution, installation, usage, etc. of this package, please see attached manual.

The code is described in this paper, please cite it if you use the softare:

M. Šmíd, X.Pan, and K. Falk, “X-ray spectrometer simulation code with a detailed support of mosaic crystals,” **Comput. Phys. Commun.**
262, 107811 (2021).   https://doi.org/10.1016/j.cpc.2020.107811

All files related to the code are available at:
https://codebase.helmholtz.cloud/smid55/mmpxrt

The packaging was done by using this manual: https://packaging.python.org/tutorials/packaging-projects/


## Structure of the repository:
-test cases - the folder with examples (test cases), i.e. executable python scripts,
    which are dependent on the proper installation of the mmpxrt library  
-mmpxrt_manual.odt - The manual, also quite useful file...   
-mmpxrt.pdf - Draft of a scientific paper describing the code.  
-mmpxrt - is a directory containing the pip package and all that it needs to be built.  
.|- dist - produced resulting pip packages; these are already uploaded to PiPY, so you can install the codes easily  
.|-mmpxrt - the actual folder with the script  
...|-mmpxrt.py - the actual script; Technically speaking, this is the only file conaining the code:-)   
